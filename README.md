# 回归自性Web端

## 简介：
```
回归自性，就要舍下自己的一切私心和欲望，发现不被外物质生活环境所驱使的真正的快乐，而自性就是最真实的自己……
```

## 技术框架
```
vue3、bootstrap3、element-plus、axios
```

## 演示地址

<a href="http://zhaoyun.site/" target="_blank"> 回归自性 </a>


## 项目搭建

### 项目安装
```
npm install
```

### 开发模式下进行编译运行、热加载
```
npm run serve
```

### 生产环境构建项目
```
npm run build
```

### 检查代码
```
npm run lint
```

