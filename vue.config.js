const webpack = require('webpack')
module.exports = {
	lintOnSave: false,
	publicPath: '/hgzx/',
	devServer: {
		// host: 'localhost',
		// port: 8081,
		// https: true,
		disableHostCheck: true,
		//open: true, //默认是否打开浏览器
		proxy: {
			'/route': {
				target: process.env.VUE_APP_API_URL,
				ws: true,
				changeOrigin: true,
				pathRewrite: {
					'^/route': ''
				}
			}
		}
	},
	configureWebpack: {
		plugins: [
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery",
				"windows.jQuery": "jquery",
				Popper: ['popper.js', 'default']
			})
		]
	},
	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = '回归自性 | 中国'
				return args
			})
	}
}
