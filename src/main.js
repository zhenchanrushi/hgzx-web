import 'babel-polyfill'
require('es6-promise').polyfill()
import Es6Promise from 'es6-promise'
Es6Promise.polyfill()

import {
	createApp
} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'jquery'
import 'jquery-slimscroll'
import 'bootstrap'
import 'bootstrap-slider'
import '@/assets/js/app.js'
// import '@/assets/js/app.plugin.js'
import api from '@/api' //数据接口API

import 'element-plus/lib/theme-chalk/index.css'
import "bootstrap/dist/css/bootstrap.min.css"
import "font-awesome/css/font-awesome.min.css"
import "@/assets/css/simple-line-icons.css"
import "@/assets/css/app.css"

import ElementPlus from 'element-plus';
import locale from 'element-plus/lib/locale/lang/zh-cn'
import VideoPlayer from 'vue-video-player' //视频播放器
import CKEditor from '@ckeditor/ckeditor5-vue'; // 富文本编辑器

import BtnGoback from '@/components/BtnGoback' //返回上一页
import MusicPlayer from '@/components/MusicPlayer'
// import Pagination from '@/components/Pagination' //分页
import FileUpload from '@/components/FileUpload' //文件上传
import ImageUpload from '@/components/ImageUpload' //文件上传


var app = createApp(App)
app.use(router)
app.use(store)

app.use(ElementPlus, {
	locale,
	size: 'small',
	zIndex: 3000
})
app.use(VideoPlayer)
app.use(CKEditor)

app.component('BtnGoback', BtnGoback)
app.component('MusicPlayer', MusicPlayer)
// app.component('Pagination', Pagination)
app.component('FileUpload', FileUpload)
app.component('ImageUpload', ImageUpload)

app.config.globalProperties.$api = api;

app.mount('#app')

/**
 * 动态路由
 */
api.menu.getTreeByUser(store.state.auth.userId).then(async (menuTree) => {
	await router.initRoutes(menuTree);
	await router.replace(router.currentRoute.value.fullPath)
})
