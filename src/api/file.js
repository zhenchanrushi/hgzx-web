import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/hgzx/file");

curd.url = {
	view: "/route/hgzx/file/view/",
	delete: "/hgzx/file/delete/",
	upload: "/hgzx/file/upload/"
}

curd.queryByBookId = (bookId) => {
	return http.get(curd.root, {
		params: {
			bookId: bookId
		}
	});
}

curd.upload = (data) => {
	let param = new FormData(); // 创建form对象
	param.append('localFile', data); // 将文件存入file下面
	let config = {
		headers: {
			'Content-Type': 'multipart/form-data'
		}
	}
	return http.post(curd.url.upload, param, config);
}

export default curd;
