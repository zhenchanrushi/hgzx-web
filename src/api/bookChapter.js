import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/hgzx/book-chapter");

curd.queryByBookId = (bookId) => {
	return http.get(curd.root + '/list', {
		params: {
			bookId: bookId
		}
	});
}

export default curd;
