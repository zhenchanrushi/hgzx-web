import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/hgzx/media-statistics");

curd.like = (id, like) => {
	return http.post(curd.root + '/like', {
		id: id,
		like: like //0:取消,1:点赞
	});
}

export default curd;
