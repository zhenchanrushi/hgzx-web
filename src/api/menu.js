import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/authority/menu");

curd.tree = (params) => {
	return http.get(curd.root + '/tree', {
		params: params
	});
}

curd.getTreeByUser = (userId) => {
	return http.get(curd.root + '/getTreeByUser', {
		params: {
			userId: userId
		}
	});
}

export default curd;
