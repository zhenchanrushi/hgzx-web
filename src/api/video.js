import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/hgzx/video");

curd.recommendedList = (videoId) => {
	return http.get(curd.root + '/list/recommended', {
		params: {
			id: videoId
		}
	});
}

/**
 * 视频采集页面解析
 */
curd.collectPageParse = (params) => {
	return http.post(curd.root + '/collect/parse', params);
}

/**
 * 视频采集
 */
curd.collectVideos = (params) => {
	return http.post(curd.root + '/collect', params);
}

export default curd;
