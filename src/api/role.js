import Curd from './base/curd'
import http from './base/axios'
import {
	ElMessage
} from 'element-plus';

var curd = new Curd("/authority/role");

curd.getMenuIdsOfRole = (roleId) => {
	return http.get(curd.root + '/menuIds', {
		params: {
			roleId: roleId
		}
	});
}

curd.setMenuOfRole = (roleId, menuIds) => {
	return http.post(curd.root + '/menuIds', {
		roleId: roleId,
		menuIds: menuIds
	});
}
export default curd;
