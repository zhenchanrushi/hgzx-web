import Curd from './base/curd'
import http from './base/axios'
import api from './index'

var curd = new Curd("/authority/sys-dict");

curd.queryDictItems = (dictCode) => {
	return api.sysDictItem.list({
		dictCode: dictCode
	});
}

export default curd;
