import Curd from './base/curd'
import auth from './auth'
import user from './user'
import menu from './menu'
import role from './role'
import sysDict from './sysDict'
import bookChapter from './bookChapter'
import file from './file'
import video from './video'
import mediaStatistics from './mediaStatistics'
import userMediaFavor from './userMediaFavor'

const api = {
	//鉴权API
	auth,
	sysDict,
	sysDictItem: new Curd("/authority/sys-dict-item"),
	file,
	menu,
	api: new Curd("/authority/api"),
	org: new Curd("/authority/org"),
	user,
	role,
	
	//回归自性API
	media: new Curd("/hgzx/media"),
	mediaStatistics,
	audio: new Curd("/hgzx/audio"),
	video,
	videoItem: new Curd("/hgzx/video-item"),
	book: new Curd("/hgzx/book"),
	bookChapter,
	bookAudio: new Curd("/hgzx/book-audio"),
	album: new Curd("/hgzx/album"),
	userMediaFavor,
	bookmark: new Curd("/hgzx/bookmark"),
	bookmarkItem: new Curd("/hgzx/bookmark-item")
}

export default api;
