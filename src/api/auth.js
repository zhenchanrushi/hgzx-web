import http from './base/axios'

export default {
	login(data) {
		return http.post('/authority/oauth/password', data).then(data => {
			console.log(data);
			return data;
		});
	},
	loginByQQ() {
		var url = "https://graph.qq.com/oauth2.0/authorize"
		var params = "?response_type=code" // 获取回access_token
		params += "&client_id=101274723" // 应用的appid
		params += "&scope=get_user_info,list_album,upload_pic,do_like" //请求用户授权列表
		params += "&state="  + location.href //移动端会丢失redirect参数，用state来替代
		params += "&redirect_uri=http://zhaoyun.site/login/qq?redirect=" + location.href
		var qqAuthUrl = url + params;
		//方案一， 页面跳转授权
		window.location.href = qqAuthUrl;
		return;
	}
}
