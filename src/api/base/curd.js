import http from './axios'
import {
	ElMessage
} from 'element-plus';

function curd(root) {
	this.root = root;

	this.query = (params) => {
		return http.get(root, {
			params: params
		});
	};

	this.list = (params) => {
		return http.get(this.root + '/list', {
			params: params
		});
	};

	this.queryById = (id) => {
		return http.get(this.root + '/detail', {
			params: {
				id: id
			}
		});
	};

	this.add = (params) => {
		return http.post(this.root + '/save', params).then((data) => {
			ElMessage({
				message: '保存成功',
				type: 'success'
			});
			return data;
		});
	};

	this.update = (params) => {
		return http.put(this.root + '/update', params).then(() => {
			ElMessage({
				message: '更新成功',
				type: 'success'
			});
		});
	};

	this.delete = (id) => {
		return http.delete(this.root + '/delete', {
			params: {
				id: id
			}
		}).then(() => {
			ElMessage({
				message: '删除成功',
				type: 'success'
			});
		});
	};

	this.batchDelete = (ids) => {
		return http.delete(this.root + '/batchDelete', {
			data: ids
		}).then(() => {
			ElMessage({
				message: '删除成功',
				type: 'success'
			});
		});
	};

}

export default curd;
