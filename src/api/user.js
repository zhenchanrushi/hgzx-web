import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/authority/user");

curd.getByToken = () => {
	return http.get(curd.root + '/loggedInUser');
}

export default curd;
