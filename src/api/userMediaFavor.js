import Curd from './base/curd'
import http from './base/axios'

var curd = new Curd("/hgzx/user-media-favor");

curd.getUserFavorsForOneMedia = (userId, mediaId) => {
	return http.get(curd.root + '/getUserFavorsForOneMedia', {
		params: {
			userId: userId,
			mediaId: mediaId
		}
	});
}

export default curd;
