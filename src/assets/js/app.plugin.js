+function ($) { "use strict";

  $(function(){
 	
	// sparkline
	var sr, sparkline = function(jQueryre){
		jQuery(".sparkline").each(function(){
			var jQuerydata = jQuery(this).data();
			if(jQueryre && !jQuerydata.resize) return;
			(jQuerydata.type == 'pie') && jQuerydata.sliceColors && (jQuerydata.sliceColors = eval(jQuerydata.sliceColors));
			(jQuerydata.type == 'bar') && jQuerydata.stackedBarColor && (jQuerydata.stackedBarColor = eval(jQuerydata.stackedBarColor));
			jQuerydata.valueSpots = {'0:': jQuerydata.spotColor};
			jQuery(this).sparkline('html', jQuerydata);
		});
	};
	jQuery(window).resize(function(e) {
		clearTimeout(sr);
		sr = setTimeout(function(){sparkline(true)}, 500);
	});
	sparkline(false);

	// easypie
	var easypie = function(){
	jQuery('.easypiechart').each(function(){
		var jQuerythis = jQuery(this), 
		jQuerydata = jQuerythis.data(), 
		jQuerystep = jQuerythis.find('.step'), 
		jQuerytarget_value = parseInt(jQuery(jQuerydata.target).text()),
		jQueryvalue = 0;
		jQuerydata.barColor || ( jQuerydata.barColor = function(jQuerypercent) {
	        jQuerypercent /= 100;
	        return "rgb(" + Math.round(200 * jQuerypercent) + ", 200, " + Math.round(200 * (1 - jQuerypercent)) + ")";
	    });
		jQuerydata.onStep =  function(value){
			jQueryvalue = value;
			jQuerystep.text(parseInt(value));
			jQuerydata.target && jQuery(jQuerydata.target).text(parseInt(value) + jQuerytarget_value);
		}
		jQuerydata.onStop =  function(){
			jQuerytarget_value = parseInt(jQuery(jQuerydata.target).text());
			jQuerydata.update && setTimeout(function() {
		        jQuerythis.data('easyPieChart').update(100 - jQueryvalue);
		    }, jQuerydata.update);
		}
			jQuery(this).easyPieChart(jQuerydata);
		});
	};
	easypie();
  
	// datepicker
	jQuery(".datepicker-input").each(function(){ jQuery(this).datepicker();});

	// dropfile
	jQuery('.dropfile').each(function(){
		var jQuerydropbox = jQuery(this);
		if (typeof window.FileReader === 'undefined') {
		  jQuery('small',this).html('File API & FileReader API not supported').addClass('text-danger');
		  return;
		}

		this.ondragover = function () {jQuerydropbox.addClass('hover'); return false; };
		this.ondragend = function () {jQuerydropbox.removeClass('hover'); return false; };
		this.ondrop = function (e) {
		  e.preventDefault();
		  jQuerydropbox.removeClass('hover').html('');
		  var file = e.dataTransfer.files[0],
		      reader = new FileReader();
		  reader.onload = function (event) {
		  	jQuerydropbox.append(jQuery('<img>').attr('src', event.target.result));
		  };
		  reader.readAsDataURL(file);
		  return false;
		};
	});

	// slider
	jQuery('.slider').each(function(){
		jQuery(this).slider();
	});

	// sortable
	if (jQuery.fn.sortable) {
	  jQuery('.sortable').sortable();
	}

	// slim-scroll
	jQuery('.no-touch .slim-scroll').each(function(){
		var jQueryself = jQuery(this), jQuerydata = jQueryself.data(), jQueryslimResize;
		jQueryself.slimScroll(jQuerydata);
		jQuery(window).resize(function(e) {
			clearTimeout(jQueryslimResize);
			jQueryslimResize = setTimeout(function(){jQueryself.slimScroll(jQuerydata);}, 500);
		});
    jQuery(document).on('updateNav', function(){
      jQueryself.slimScroll(jQuerydata);
    });
	});	

	// portlet
	jQuery('.portlet').each(function(){
		jQuery(".portlet").sortable({
	        connectWith: '.portlet',
            iframeFix: false,
            items: '.portlet-item',
            opacity: 0.8,
            helper: 'original',
            revert: true,
            forceHelperSize: true,
            placeholder: 'sortable-box-placeholder round-all',
            forcePlaceholderSize: true,
            tolerance: 'pointer'
	    });
    });

	// docs
  jQuery('#docs pre code').each(function(){
	    var jQuerythis = jQuery(this);
	    var t = jQuerythis.html();
	    jQuerythis.html(t.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
	});

	// table select/deselect all
	jQuery(document).on('change', 'table thead [type="checkbox"]', function(e){
		e && e.preventDefault();
		var jQuerytable = jQuery(e.target).closest('table'), jQuerychecked = jQuery(e.target).is(':checked');
		jQuery('tbody [type="checkbox"]',jQuerytable).prop('checked', jQuerychecked);
	});

	// random progress
	jQuery(document).on('click', '[data-toggle^="progress"]', function(e){
		e && e.preventDefault();

		var jQueryel = jQuery(e.target),
		jQuerytarget = jQuery(jQueryel.data('target'));
		jQuery('.progress', jQuerytarget).each(
			function(){
				var jQuerymax = 50, jQuerydata, jQueryps = jQuery('.progress-bar',this).last();
				(jQuery(this).hasClass('progress-xs') || jQuery(this).hasClass('progress-sm')) && (jQuerymax = 100);
				jQuerydata = Math.floor(Math.random()*jQuerymax)+'%';
				jQueryps.css('width', jQuerydata).attr('data-original-title', jQuerydata);
			}
		);
	});
	
	// add notes
	function addMsg(jQuerymsg){
		var jQueryel = jQuery('.nav-user'), jQueryn = jQuery('.count:first', jQueryel), jQueryv = parseInt(jQueryn.text());
		jQuery('.count', jQueryel).fadeOut().fadeIn().text(jQueryv+1);
		jQuery(jQuerymsg).hide().prependTo(jQueryel.find('.list-group')).slideDown().css('display','block');
	}
	var jQuerymsg = '<a href="#" class="media list-group-item">'+
                  '<span class="pull-left thumb-sm text-center">'+
                    '<i class="fa fa-envelope-o fa-2x text-success"></i>'+
                  '</span>'+
                  '<span class="media-body block m-b-none">'+
                    'Sophi sent you a email<br>'+
                    '<small class="text-muted">1 minutes ago</small>'+
                  '</span>'+
                '</a>';	
  setTimeout(function(){addMsg(jQuerymsg);}, 1500);

	//chosen
	jQuery(".chosen-select").length && jQuery(".chosen-select").chosen();

  });
}(jQuery);