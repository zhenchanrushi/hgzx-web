import {
	getCache,
	setCache,
	removeCache
} from "@/utils/cache";

const state = {
	count: 0,
	prePage: {}
}

const getters = {
	prePage: state => {
		return state.prePage;
	}
}

const mutations = {
	prePage: (state, prePage) => {
		state.prePage = prePage;
	}
}

const actions = {}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
