import { createStore } from 'vuex'
import auth from './modules/auth.js'
import goback from './modules/goback.js'

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth: auth,
	goback: goback
  }
})
