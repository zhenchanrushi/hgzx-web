import Layout from '@/layout/index.vue'
import home from '@/views/home'

var defaultRoutes = [{
			path: '/',
			name: 'Layout',
			// redirect: '/',
			component: Layout,
			meta: {
				title: "明心见性",
				hidden: false
			},
			children: [{
				path: '/',
				name: 'Home',
				component: home,
				meta: {
					title: "首页",
					icon: 'icon-home icon text-dark',
					hidden: false
				}
			}]
		},
		{
			path: '/register',
			name: 'register',
			component: () => import('@/views/register'),
			meta: {
				title: "注册",
				hidden: true
			}
		},
		{
			path: '/login',
			name: 'login',
			component: () => import('@/views/login'),
			meta: {
				title: "登录",
				hidden: true
			}
		},
		{
			path: '/logout',
			name: 'logout',
			meta: {
				title: "退出",
				hidden: true
			},
			beforeEnter(to, from, next) {
				// auth.logout()
				next('/')
			}
		},
		{
			path: '/404',
			name: '404',
			component: () => import('@/views/404'),
			meta: {
				title: "404",
				hidden: true
			}
		}
		
		// ,
		// {
		// 	path: "/:pathMatch(.*)*",
		// 	name: '404',
		// 	component: () => import('@/views/404'),
		// 	meta: {
		// 		title: "404",
		// 		hidden: true
		// 	}
		// }
	]

export default defaultRoutes