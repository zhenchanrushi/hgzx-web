import Layout from '@/layout/index.vue'
const defaultRoutes = [{
		path: '/',
		name: 'Home',
		// redirect: '/',
		component: Layout,
		meta: {
			title: "明心见性",
			hidden: false
		},
		children: [{
				path: '/',
				name: 'Home',
				component: () => import('@/views/home'),
				meta: {
					title: "首页",
					icon: 'icon-home icon text-dark',
					hidden: false
				}
			},
			{
				name: 'events',
				path: '/events',
				component: () => import('@/views/events'),
				meta: {
					title: "发现",
					icon: 'icon-frame icon text-success',
					messageNum: 8,
					hidden: false
				}
			},
			{
				name: 'dplayer',
				path: '/dplayer',
				component: () => import('@/views/dplayer'),
				meta: {
					title: "DPlayer",
					icon: 'icon-frame icon text-success',
					messageNum: 8,
					hidden: false
				}
			},
			{
				name: 'listen',
				path: '/listen',
				component: () => import('@/views/listen'),
				meta: {
					title: "听",
					icon: 'icon-music-tone-alt icon text-info-dker',
					messageNum: null,
					hidden: false
				}
			},
			{
				name: 'video',
				path: '/video',
				component: () => import('@/views/video'),
				meta: {
					title: "看",
					icon: 'icon-social-youtube icon text-primary-lter', //fa fa-video-camera
					messageNum: null,
					hidden: false
				}
			},
			{
				name: 'videoDetail',
				path: '/video/:id',
				component: () => import('@/views/video-detail'),
				meta: {
					title: "视频",
					icon: 'icon-social-youtube icon text-primary',
					hidden: true
				}
			},
			{
				name: 'book',
				path: '/book',
				component: () => import('@/views/book'),
				meta: {
					title: "读",
					icon: 'icon-notebook icon text-success-dker',
					messageNum: null,
					hidden: false
				}
			},
			{
				name: 'bookChapter',
				path: '/book/:id',
				component: () => import('@/views/book-chapter'),
				meta: {
					title: "章节",
					icon: 'icon-list icon text-info-lter',
					messageNum: null,
					hidden: true
				}
			},
			{
				name: 'org',
				path: '/admin/org',
				component: () => import('@/views/admin/org'),
				meta: {
					title: "组织机构",
					icon: 'fa fa-group text-default',
					messageNum: null,
					hidden: false
				}
			}, {
				name: 'user',
				path: '/admin/user',
				component: () => import('@/views/admin/user'),
				meta: {
					title: "用户管理",
					icon: 'fa fa-user-o text-default',
					messageNum: null,
					hidden: false
				}
			}, {
				name: 'role',
				path: '/admin/role',
				component: () => import('@/views/admin/role'),
				meta: {
					title: "角色配置",
					icon: 'fa fa-balance-scale text-default',
					messageNum: null,
					hidden: false
				}
			}, {
				name: 'menu',
				path: '/admin/menu',
				component: () => import('@/views/admin/menu'),
				meta: {
					title: "菜单管理",
					icon: 'fa fa-list text-default',
					messageNum: null,
					hidden: false
				}
			}, {
				name: 'api',
				path: '/admin/api',
				component: () => import('@/views/admin/api'),
				meta: {
					title: "接口管理",
					icon: 'fa fa-link text-default',
					messageNum: null,
					hidden: false
				}
			}, {
				name: 'media',
				path: '/admin/media',
				component: () => import('@/views/admin/media'),
				meta: {
					title: "媒体管理",
					icon: 'fa fa-film text-default',
					messageNum: null,
					keepAlive: true,
					hidden: false
				}
			},
			{
				name: 'audioForm',
				path: '/admin/form/audio/:id',
				props: true,
				component: () => import('@/views/admin/audioForm'),
				meta: {
					title: "表单：音频",
					icon: 'fa fa-edit text-default',
					hidden: true,
					messageNum: null
				}
			},
			{
				name: 'videoForm',
				path: '/admin/form/video/:id',
				props: true,
				component: () => import('@/views/admin/videoForm'),
				meta: {
					title: "表单：视频",
					icon: 'fa fa-edit text-default',
					hidden: true,
					messageNum: null
				}
			},
			{
				name: 'bookForm',
				path: '/admin/form/book/:id',
				props: true,
				component: () => import('@/views/admin/bookForm'),
				meta: {
					title: "表单：图书",
					icon: 'fa fa-edit text-default',
					hidden: true,
					messageNum: null
				}
			},
			{
				name: 'pictureForm',
				path: '/admin/form/picture/:id',
				props: true,
				component: () => import('@/views/admin/pictureForm'),
				meta: {
					title: "表单：图片",
					icon: 'fa fa-edit text-default',
					hidden: true,
					messageNum: null
				}
			}, {
				name: 'sysDict',
				path: '/sys-dict',
				component: () => import('@/views/admin/sysDict'),
				meta: {
					title: "系统字典",
					icon: 'fa fa-book fa-lg text-info',
					messageNum: null,
					hidden: false
				}
			},
			{
				name: 'album',
				path: '/admin/album',
				component: () => import('@/views/admin/album'),
				meta: {
					title: "专辑",
					icon: 'fa fa-registered text-success-dker',
					messageNum: null,
					hidden: false
				}
			},
			{
				name: 'profile',
				path: '/user/profile',
				component: () => import('@/views/user/profile'),
				meta: {
					title: "我的",
					icon: 'fa fa-user-o text-default',
					messageNum: null,
					hidden: false
				}
			}
		]
	},
	{
		path: '/register',
		name: 'register',
		component: () => import('@/views/register'),
		meta: {
			title: "注册",
			hidden: true
		}
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('@/views/login'),
		meta: {
			title: "登录",
			hidden: true
		}
	},
	{
		path: '/logout',
		name: 'logout',
		meta: {
			title: "退出",
			hidden: true
		},
		beforeEnter(to, from, next) {
			// auth.logout()
			next('/')
		}
	},
	{
		path: "/:pathMatch(.*)*",
		name: '404',
		component: () => import('@/views/404'),
		meta: {
			title: "404",
			hidden: true
		}
	}
]

export default defaultRoutes
