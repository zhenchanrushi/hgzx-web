import home from '@/views/home'
import events from '@/views/events'
import dplayer from '@/views/dplayer'
import listen from '@/views/listen'
import video from '@/views/video'
import videoDetail from '@/views/video-detail'
import book from '@/views/book'
import bookChapter from '@/views/book-chapter'

import org from '@/views/admin/org'
import user from '@/views/admin/user'
import role from '@/views/admin/role'
import menu from '@/views/admin/menu'
import api from '@/views/admin/api'
import media from '@/views/admin/media'
import audioForm from '@/views/admin/audioForm'
import videoForm from '@/views/admin/videoForm'
import bookForm from '@/views/admin/bookForm'
import pictureForm from '@/views/admin/pictureForm'
import sysDict from '@/views/admin/sysDict'
import album from '@/views/admin/album'
import profile from '@/views/user/profile'

var components = {
	home,
	events,
	listen,
    dplayer,
	video,
	videoDetail,
	book,
	bookChapter,
	org,
	user,
	role,
	menu,
	api,
	media,
	audioForm,
	videoForm,
	bookForm,
	pictureForm,
	sysDict,
	album,
	profile
}

export default components